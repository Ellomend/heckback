import { WebClient } from '../../src/efscraper/WebClient'
import { efscraperSettings } from '../../src/efscraper/Settings'
import { HtmlParser } from '../../src/efscraper/HtmlParser'
import { efscraperStub } from '../stubs/efscraper/efscraperStubs'
import { EnglishFilmsClient } from '../../src/efscraper/EnglishFilmsClient'

describe("Test efscraper is working ", () => {


  describe("Test web client.", () => {

    it("webclient returns html string", async () => {
      const htmlString = await WebClient.getByUrl(efscraperSettings.baseUrl + efscraperSettings.newMoviesUrl)
      expect(typeof htmlString).toBe('string')
    });

  });

  describe("Test html parser.", () => {

    it("html parser parses movies", async () => {
      const htmlText = efscraperStub.pageShortStub
      const titles = await HtmlParser.getMovies(htmlText)
      console.log(titles)
      expect(titles.length).toBe(4)
    });

  });

  describe("Test English Films Client.", () => {
    it("it fetches movies", async () => {
      const movies = await EnglishFilmsClient.get2019Movies()
      console.log(movies)
      expect(movies.length).toBe(15)
    });

  });
});
