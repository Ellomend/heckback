FROM mhart/alpine-node:12
RUN mkdir /hecklists
WORKDIR /hecklists
# deps
# python a
#RUN apk add --no-cache --virtual .gyp python make g++


#COPY package.json .
COPY . .
#RUN npm i -g moleculer-cli
RUN npm i -g moleculer-cli
#RUN npm install --loglevel verbose
RUN yarn

# build
RUN npm run clear
RUN npm run build
EXPOSE 4002
#CMD ["npm", "run", "prodrepl"]
#moleculer connect --config ./moleculer.config.js
CMD ["npm", "run", "prodrepl"]
