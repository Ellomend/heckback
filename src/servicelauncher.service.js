import { SRV_ACCOUNTS } from './lib/graphql-services/accounts'
import { SRV_INVENTORY } from './lib/graphql-services/inventory'
import { SRV_PRODUCTS } from './lib/graphql-services/products'
import { SRV_REVIEWS } from './lib/graphql-services/reviews'
import SRV_YOGAAUTH from './lib/graphql-services/yogaauth'
import { SRV_INTEREST } from './lib/graphql-services/interest'
const serviceServers = [
  SRV_ACCOUNTS,
  SRV_INVENTORY,
  SRV_PRODUCTS,
  SRV_REVIEWS,
  SRV_YOGAAUTH,
  SRV_INTEREST
];

export const LAUNCHER = {

  name: "servicelauncher",
  settings: {
    $dependencyTimeout: 30000 // Default: 0 - no timeout
  },
  dependencies: [
    'federation'
  ],
  methods: {
    async addToFederation (url, name) {
      console.log(`Launcher: Att Adding apollo server to federation: ${url} ${name}`)
      return new Promise(async (resolve) => {
        setTimeout(() => {
          this.broker.call('federation.add', {
            url,
            name
          },{
            timeout: 33000
          }).then(r => {
            console.log('Launcher: Adding to federation seems to be ok.')
            resolve(true)
          })
        }, 300)
      })

    }
  },
  async started () {
    let START_SERVER_PORT = 5001;
    const STEP = 10;
    for (const server of serviceServers) {
      START_SERVER_PORT = START_SERVER_PORT + STEP;
      await server._go({ port: START_SERVER_PORT }).then(async ({ url }) => {
        console.log(`🚀 Apollo Service Server started at ${url} :: ${server._name}`);
        await this.addToFederation(url, server._name)
      });
    }
    console.log('Launcher: restarting server')
    this.broker.call('federation.start', {},{
      timeout: 33000
    }).then(r => {
      console.log('Launcher: Restarting seems to be ok.')
    })
  }
};
export default LAUNCHER;

module.exports = LAUNCHER;
