import axios from 'axios'
import iconv from 'iconv-lite'

export class WebClient {
  static async getByUrl(url: string) : Promise<string> {
    let res = await axios.get(url,{
      responseType: 'arraybuffer'
    })
    return iconv.decode(res.data, 'win1251')
  }
}
