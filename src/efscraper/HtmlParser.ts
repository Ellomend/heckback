import cheerio from 'cheerio'
import { Movie } from "./types"

export const HtmlParser = {
  getTextByTag (tag: string, html: string): Array<string> {
    return []
  },
  async getMovies(html: string): Promise<Array<Movie>> {
    const wrapper = await cheerio.load(html, { decodeEntities: false });
    let h2textList : Array<Movie> = []
    wrapper('article.shortNews').each((i, el) => {
      h2textList.push(this.getMovie(el))
    })
    return h2textList
  },
  /**
   * extract title
   * @param el
   */
  getTitle(el: Element): string {
    let titleEl = cheerio(el).find('meta[itemprop="name"]')
    if (titleEl) {
      return cheerio(titleEl).attr('content')
    } else {
      return 'No title'
    }
  },
  /**
   * extract image url
   * @param el
   */
  getImageUrl (el: Element): string {
    let imageEl = cheerio(el).find('meta[itemprop="image"]')
    if (imageEl) {
      return cheerio(imageEl).attr('content')
    } else {
      return ''
    }
  },
  /**
   * extract movie url
   * @param el
   */
  getMovieUrl(el: Element) : string {
    let movieLink = cheerio(el).find('h2.newsTitle>a')
    if (movieLink) {
      return cheerio(movieLink).attr('href')
    } else {
      return ''
    }
  },
  getMovie (el: Element) : Movie {
    return {
      url: this.getMovieUrl(el),
      title: this.getTitle(el),
      image: this.getImageUrl(el)
    }
  }
}
