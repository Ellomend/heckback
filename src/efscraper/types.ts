export interface Movie {
  title: string
  url: string
  image: string
}
