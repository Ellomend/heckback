import { WebClient } from "./WebClient"
import { efscraperSettings } from "./Settings"
import { HtmlParser } from "./HtmlParser"
import { Movie } from "./types"

export class EnglishFilmsClient {
  static async get2019Movies () : Promise<Array<Movie>> {
    let html = await WebClient.getByUrl(efscraperSettings.baseUrl + efscraperSettings.newMoviesUrl)
    let list = await HtmlParser.getMovies(html)
    return list
  }
}
