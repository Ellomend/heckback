import { EnglishFilmsClient } from "./EnglishFilmsClient"

export class EFScraper {
  public static async fetch2019movies() {
    let res = await EnglishFilmsClient.get2019Movies()
    return res
  }
}
