import { FederationServer } from './lib/federation/federationServer'

export const FED_SERVICES = {

  name: "federation",

  actions: {
    async add(ctx) {
      console.log(`Federation: adding server`)
      console.log(ctx.params)
      let serviceAdded = await this.$ff.addService(ctx.params)
      console.log('Federation: Added')
      return !!serviceAdded
    },
    // call federation.stop
    async stop () {
      console.log(`Federation: stopping server`)
      await this.$ff.stopServer('just stop')
      return true
    },
    // call federation.start
    async start () {
      console.log(`Federation: starting server`)
      await this.$ff.startServer('just start')
      return true
    },
    // call federation.restart
    async restart () {
      console.log(`Federation: restarting server`)
      await this.$ff.stopServer('just stop')
      await this.$ff.startServer('just start')
      return true
    }
  },

  async created() {
    this.$ff = new FederationServer()

  },
  async started () {
  }

}


export default FED_SERVICES;
module.exports = FED_SERVICES;
