const ApiGateway = require("moleculer-web");
export const API_SERVICE =  {

	name: "api",
	mixins: [ApiGateway],

	settings: {
		routes: [{
			path: "/api",
			whitelist: [
				// Access to any actions in all services under "/api" URL
				"**"
			],
		}],

		// Serve assets from "public" folder
		assets: {
			folder: "public"
		},
		port: 4003
	},
	actions: {
		hello() {
			return "Hello API Gateway!"
		}
	}

};

export default API_SERVICE;
module.exports = API_SERVICE;
