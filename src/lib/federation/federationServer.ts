import { ServicesStore } from "./federationGateway"
import { ApolloServer } from "apollo-server"
import { ApolloService } from "./types"

export class FederationServer {
  public servicesStore: ServicesStore
  public server?: ApolloServer
  private port: number

  constructor() {
    this.servicesStore = new ServicesStore()
    this.port = 4002
  }

  async startServer(service = 'no service') {
    return new Promise(async resolve => {
      let ready = await this.checkServices()
      console.log('ready')
      if (!ready) {
        resolve(false)
      }
      await this.stopServer(service)
      console.log('stopped')
      const {schema, executor} = await this.servicesStore.getServerConfig()
      console.log(this.servicesStore.services)
      console.log('config ok, start server')

      this.server = undefined
      this.server = new ApolloServer({
        schema,
        executor,
        context: (request) => {
          // build context for gateway request here
          console.log('Federation server CONTEXT')
          // get auth token
          const token = request.req.headers.authorization || '';
          // add the user to the context
          return {
            authToken: token
          }
        },
      });
      console.log('starting')
      await this.server.listen({port: this.port}).then(({url}) => {
        // this.port = this.port + 1
        console.log('Federation: Restarted.')
        console.log(`Federation: ${url} Port: ${this.port}.`)
        resolve(true)
      });
    })
  }

  async stopServer(name: string = 'no svc') {
    return new Promise(async (resolve, reject) => {
      if (this.server) {
        console.log('stopping f server')
        setTimeout(() => {
          console.log(`>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>timeouted stopd :${name}`)
          resolve(true)
        }, 9000)
        await this.server.stop()
          .then(r => {
            console.log(`stopped f server: ${name}`)
            resolve(true)
          })
          .catch(err => {
            console.log('failed to stop f server')
            reject(false)
          })
      } else {
        resolve(true)
      }
    })
  }

  async addService(service: ApolloService) {
    let added = await this.servicesStore.addService(service)
    console.log('added to store')
    if (added) {
      // await this.stopServer()
      // await this.startServer(service.name)
      console.log('added to store no restart')
    }
  }

  checkServices() {
    return this.servicesStore.checkServices()
  }
}
