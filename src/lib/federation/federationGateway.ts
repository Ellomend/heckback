import { ApolloGateway, RemoteGraphQLDataSource } from '@apollo/gateway'
import { ApolloService } from "./types"
import ApplicationConfig from "../../config/main.conf"


export const gatewayFactory = (servicesList : ApolloService[]) : ApolloGateway => {
  return new ApolloGateway({
    serviceList: servicesList,
    buildService({ name, url }) {
      return new RemoteGraphQLDataSource({
        url,
        willSendRequest({ request, context }) {
          // prepare headers for child services here
          // add token to headers for child services
          console.log('Federation gateway context')
          request.http.headers.set(ApplicationConfig.variables.authHeaderKey, context.authToken);
          request.authToken = context.authToken
        }
      })
    }
  })
}

export const loadGateway = async (gateway : ApolloGateway) => {
  const { schema, executor } = await gateway.load()
    .catch(err => {
      throw err
    });
  return {schema, executor}
}
export const checkService = async (service : ApolloService) => {
  let res = await gatewayFactory([service]).load()
  console.log('F: check service complete')
  return !!res;
}

export class ServicesStore {
  public services: ApolloService[]
  constructor () {
    this.services = []
  }

  async checkServices () {
    return new Promise(async (resolve, reject) => {
      if (!this.services || !this.services.length) {
        resolve(false)
      }
      let res = await gatewayFactory(this.services).load()
        .catch(err => {
        })
      console.log('checking service OK')
      resolve(true)
    })
  }

  async getServerConfig() {
    console.log('loading')
    const { schema, executor } = await gatewayFactory(this.services).load()
    console.log('loaded')
    return { schema, executor }
  }

  async addService(service: ApolloService) {
    let valid = await checkService(service)
    if (valid) {
      this.services.push(service)
      return true
    }
    return false
  }

  async removeService(service: ApolloService) {
    const foundService = this.services.find(svc => {
      return svc.name = service.name
    })
    if (foundService) {
      this.services = this.services.filter(svc => {
        return svc.name !== service.name
      })
    }
  }
}
