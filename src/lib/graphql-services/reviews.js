const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");

const typeDefs = gql`
    extend type Query {
        reviews: [Review]
    }
    type Review @key(fields: "id") {
        id: String
        body: String
    }
`;

const resolvers = {
  Query: {
    reviews() {
      return reviews;
    }
  }
};

const server = new ApolloServer({
  schema: buildFederatedSchema([
    {
      typeDefs,
      resolvers
    }
  ])
});
server._name = 'reviews'
// server.listen({ port: 4002 }).then(({ url }) => {
//   console.log(`🚀 Server ready at ${url}`);
// });
server._go = server.listen
export const SRV_REVIEWS = server

const usernames = [
  { id: "1", username: "@ada" },
  { id: "2", username: "@complete" }
];
const reviews = [
  {
    id: "1",
    authorID: "1",
    product: { upc: "1" },
    body: "Love it!"
  },
  {
    id: "2",
    authorID: "1",
    product: { upc: "2" },
    body: "Too expensive."
  },
  {
    id: "3",
    authorID: "2",
    product: { upc: "3" },
    body: "Could be betster."
  },
  {
    id: "4",
    authorID: "2",
    product: { upc: "1" },
    body: "Prefer something else."
  }
];
