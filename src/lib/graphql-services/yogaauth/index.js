const { GraphQLServer } = require('graphql-yoga')
const { prisma } = require('./src/generated/prisma-client')
const { resolvers } = require('./src/resolvers')
const { permissions } = require('./src/permissions')
const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");
import { applyMiddleware } from 'graphql-middleware'

const td = gql`
    scalar DateTime

    type Query {
        me: User
        feed: [Post!]!
        filterPosts(searchString: String): [Post!]!
        post(id: ID!): Post
        someObj: SOType
    }

    type SOType {
        pepe: String
        age: String
    }
    type Mutation {
        createDraft(title: String!, content: String): Post!
        deletePost(id: ID!): Post
        publish(id: ID!): Post
        signup(email: String!, password: String!, name: String): AuthPayload!
        login(email: String!, password: String!): AuthPayload!
    }

    type AuthPayload {
        token: String!
        user: User!
    }

    type Post {
        id: ID!
        createdAt: DateTime!
        updatedAt: DateTime!
        published: Boolean!
        title: String!
        content: String
        author: User!
    }

    type User {
        id: ID!
        email: String!
        name: String
        posts: [Post!]!
        username: String
    }

`

// const server1 = new GraphQLServer({
//   typeDefs: td,
//   resolvers,
//   middlewares: [permissions],
//   context: request => {
//     return {
//       ...request,
//       prisma,
//     }
//   },
// })


let schema = buildFederatedSchema([
  {
    typeDefs: td,
    resolvers,

  }
])
// Minimal example middleware (before & after)
const beepMiddleware = permissions
const schemaWithMiddleware = applyMiddleware(
  schema,
  beepMiddleware
)
const server = new ApolloServer({
  schema: schemaWithMiddleware,
  context: request => {
    return {
      ...request,
      prisma,
    }
  },

});


server._name = 'yogaauth'
server._go = server.listen

export const SRV_YOGAAUTH = server
export default SRV_YOGAAUTH
