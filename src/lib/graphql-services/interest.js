import { EFScraper } from '../../efscraper'
import { applyMiddleware } from 'graphql-middleware'
import PermissionsManager from '../../common/permissions/PermissionsManager'
import ApplicationConfig from '../../config/main.conf'
import { prisma } from './yogaauth/src/generated/prisma-client'

const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");

const typeDefs = gql`
    extend type Query {
        movies: [Movie]
    }
    type Movie {
        title: String
        image: String
        url: String
    }
`;

const resolvers = {
  Query: {
    async movies(parent, args, context) {
      // let user = await context.prisma.user({ id: userId })
      let movies = await EFScraper.fetch2019movies()
      return movies
    }
  }
};
let schema = buildFederatedSchema([
  {
    typeDefs: typeDefs,
    resolvers,

  }
])
const beepMiddleware = PermissionsManager.getPermissionsIntance()
const schemaWithMiddleware = applyMiddleware(
  schema,
  beepMiddleware
)
const server = new ApolloServer({
  schema: schemaWithMiddleware,
  context: (request) => {
    console.log('Interest server context')
    // console.log('cont', Object.keys(context))

    const token = request.req.headers[ApplicationConfig.variables.authHeaderKey] || '';
    return {
      s_interest: 'done',
      req: request.req,
      token,
      prisma,
    }
  },
});
server._name = 'interest'
// server.listen({ port: 4004 }).then(({ url }) => {
//   console.log(`🚀 Server ready at ${url}`);
// });
server._go = server.listen
export const SRV_INTEREST = server
