const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");


const typeDefs = gql`
    extend type Query {
        accounts: [Account]
    }
    type Account @key(fields: "id") {
        id: ID!
        name: String
        username: String
    }
`;

const resolvers = {
  Query: {
    accounts () {
      return users;
    }
  }
};

const server = new ApolloServer({
  schema: buildFederatedSchema([
    {
      typeDefs,
      resolvers
    }
  ])
});


server._name = 'accounts'
// server.listen({ port: 4002 }).then(({ url }) => {
//   console.log(`🚀 Server ready at ${url}`);
// });
server._go = server.listen
export const SRV_ACCOUNTS = server
const users = [
  {
    id: "1",
    name: "Ada Lovelace",
    birthDate: "1815-12-10",
    username: "@ada"
  },
  {
    id: "2",
    name: "Alan Turing",
    birthDate: "1912-06-23",
    username: "@complete"
  }
];
