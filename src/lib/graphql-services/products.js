const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");

const typeDefs = gql`
    extend type Query {
        topProducts: [Product]
    }

    type Product @key(fields: "upc") {
        upc: String!
        name: String
        price: Int
        weight: Int
    }
`;

const resolvers = {
  Query: {
    topProducts() {
      return products;
    }
  }
};

const server = new ApolloServer({
  schema: buildFederatedSchema([
    {
      typeDefs,
      resolvers
    }
  ])
});
server._name = 'products'
// server.listen({ port: 4003 }).then(({ url }) => {
//   console.log(`🚀 Server ready at ${url}`);
// });
server._go = server.listen
export const SRV_PRODUCTS = server

const products = [
  {
    upc: "1",
    name: "Table",
    price: 899,
    weight: 100
  },
  {
    upc: "2",
    name: "Couch",
    price: 1299,
    weight: 1000
  },
  {
    upc: "3",
    name: "Chair",
    price: 54,
    weight: 50
  }
];
