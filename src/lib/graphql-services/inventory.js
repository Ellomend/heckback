const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");

const typeDefs = gql`  
    extend type Query {
        inventories: [Inventory]
    }
    type Inventory {
        name: String
        content: String
    }
`;

const resolvers = {
  Query: {
    inventories() {
      return inventory
    }
  }
};

const server = new ApolloServer({
  schema: buildFederatedSchema([
    {
      typeDefs,
      resolvers
    }
  ])
});
server._name = 'inventory'
// server.listen({ port: 4004 }).then(({ url }) => {
//   console.log(`🚀 Server ready at ${url}`);
// });
server._go = server.listen
export const SRV_INVENTORY = server

const inventory = [
  { upc: "1", inStock: "1c" },
  { upc: "2", inStock: "2c" },
  { upc: "3", inStock: "3c" },
  { upc: "4", inStock: "4c" },
];
