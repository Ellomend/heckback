import { rule, and, shield } from 'graphql-shield'
import PermissionsManager from "./PermissionsManager"

const rulesInstance = {
  logUser: rule()(async (parent, args, context) => {
    // const userId = getUserId(context)
    // console.log('sss',userId)
    console.log('Permission rule')
    let userId = await PermissionsManager.getUserId(context)
    console.log('YES', userId)
    return Boolean(true)
  }),
  authOnly: rule()(async (parent, args, context) => {
    // const userId = getUserId(context)
    // console.log('sss',userId)
    console.log('auth only rule')
    let userId = await PermissionsManager.getUserId(context)
    console.log('auth only rule user id', userId)
    return Boolean(!!userId)
  }),
}

const permissions = shield({
  Query: {
    movies: and(rulesInstance.logUser,rulesInstance.authOnly),
  }
})

export default {
  permissions,
}
