import PermissionsInstance from "./PermissionsInstance"
import ApplicationConfig from "../../config/main.conf"
const { verify } = require('jsonwebtoken')

export class PermissionsManager {
  public static getPermissionsIntance = () => {
    return PermissionsInstance.permissions
  }

  public static verifyToken(token) {
    return new Promise((resolve, reject) => {
      verify(ApplicationConfig.variables.testToken, ApplicationConfig.variables.appSecret, (err, decoded) => {
        if (err) {
          console.log('failed to vrf token')
          reject(err)
        } else {
          console.log('success to vrf token')
          resolve(decoded)
        }
      })
    })
  }
  public static async getUserId(context) {
    const AuthorizationToken = context.req.get('authorization')
    if (AuthorizationToken) {
      const token = AuthorizationToken
      const verifiedToken = await this.verifyToken(AuthorizationToken)
      return verifiedToken && verifiedToken.userId
    }
  }
}
export default PermissionsManager
